# Cypress Automation
Sample cypress practise project

# Tools and Framework used

1. Framework: Cypress
2. Supporting Language: Javascript
3. Supporting Libraries:
4. Node.js (12 or Higher)
5. Mocha

# Supported Browsers
The following are the supported browsers as of this version; 7.6.0
Chrome
Edge
Electron (default)
Firefox



## Getting Started
```
1. git clone https://gitlab.com/tobyfagboh/quidax-test
2. Navigate to `quidax-test`
3. In the terminal, Perform "npm i" to install all the dependencies present in the package.json file.
```

## Runing a Test
```
1. In the terminal, perform `npx cypress runt` to start the script execution
```

### Reports used
- Mochawesome Html reporter


### Sample Report
- Report can be found in cypress/reports/index.htm
export const homepage = {
    popUpBoxNo:".at-cm-no-button",
    popUpBoxYes:".at-cm-yes-button",
    inputFromDropDown:":nth-child(1) > :nth-child(1) > .dropdown-toggle",
    radioButtonDemo:".open > .dropdown-menu > :nth-child(3) > a",
    alertAndModal:".navbar-right > :nth-child(2) > .dropdown-toggle",
    bootstrapAlert:".open > .dropdown-menu > :nth-child(1) > a",
    listBox:".navbar-right > :nth-child(3) > .dropdown-toggle",
    bootstrapListBox:".open > .dropdown-menu > :nth-child(1) > a"
}

export const radioButton = {
    maleRadioBtn:".panel-body > :nth-child(2) > input",
    getCheckedValueBtn:"#buttoncheck",
    getCheckedValueText:".radiobutton",
    groupRadioBtnFemale:".panel-body > :nth-child(2) > :nth-child(3) > input",
    ageGroup5to15:":nth-child(3) > :nth-child(3) > input",
    ageGroupGetValueBtn:".panel-body > .btn",
    ageGroupgetValueText:".groupradiobutton"
}

export const alertsAndModals = {
    autoCloseSuccBtn:"#autoclosable-btn-success",
    normalSuccMsg:"#normal-btn-success",
    alertNomral:".alert-normal-success"

}

export const bootstrapListPage = {
    searchBox:".list-left > .well > #listhead > .col-md-10 > .input-group > .form-control",
    bootStrapDaulList:".list-left > .well > .list-group > :nth-child(1)",
    moveRightBtn:".move-right > .glyphicon",
    activeRightList:".active"

}
import {homepage, alertsAndModals} from "../fixtures/selectors.js";

describe("Given I am on the selenium easy page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("I should be able to check one of the alerts and modals - Normal Messgae", function () {
      cy.get(homepage.popUpBoxNo).click()
      cy.get(homepage.alertAndModal).click()
      cy.get(homepage.bootstrapAlert).click()
      cy.get(alertsAndModals.normalSuccMsg).click()
      cy.get(alertsAndModals.alertNomral).should('include.text', "I'm a normal success message.");
    })

})
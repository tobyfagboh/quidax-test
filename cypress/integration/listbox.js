import {homepage, bootstrapListPage} from "../fixtures/selectors.js";

describe("Given I am on the selenium easy page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("I should be able to check one of the alerts and modals - Normal Messgae", function () {
      cy.get(homepage.popUpBoxNo).click()
      cy.get(homepage.listBox).click()
      cy.get(homepage.bootstrapListBox).click()
      cy.get(bootstrapListPage.searchBox).type('boot')
      cy.get(bootstrapListPage.bootStrapDaulList).click()
      cy.get(bootstrapListPage.moveRightBtn).click()
      cy.get(bootstrapListPage.activeRightList).should('include.text', "bootstrap-duallist ");
    })

})
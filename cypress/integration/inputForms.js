import {homepage, radioButton} from "../fixtures/selectors.js";

describe("Given I am on the selenium easy page", function () {
    beforeEach(function () {
        cy.visit('/')
    });

    it("I should be able to check one of the input forms - Radio Button", function () {
      cy.get(homepage.popUpBoxNo).click()
      cy.get(homepage.inputFromDropDown).click()
      cy.get(homepage.radioButtonDemo).click()
      cy.get(radioButton.maleRadioBtn).click()
      cy.get(radioButton.getCheckedValueBtn).click()
      cy.get(radioButton.getCheckedValueText).should('be.visible').should('contain', "Radio button 'Male' is checked");
      cy.get(radioButton.groupRadioBtnFemale).click()
      cy.get(radioButton.ageGroup5to15).click()
      cy.get(radioButton.ageGroupGetValueBtn).click()
      cy.get(radioButton.ageGroupgetValueText).should('be.visible').should('contain', "Sex : Female");
      cy.get(radioButton.ageGroupgetValueText).should('be.visible').should('contain', " Age group: 5 - 15");
    })




})